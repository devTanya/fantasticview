//
//  ViewController.swift
//  FantasticView
//
//  Created by Tanya Landsman on 1/23/17.
//  Copyright © 2017 Cars.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let fantasticView = FantasticView(frame: view.bounds)
        view.addSubview(fantasticView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

