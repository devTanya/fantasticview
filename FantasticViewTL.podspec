Pod::Spec.new do |s|
  s.name             = 'FantasticViewTL'
  s.version          = '0.1.0'
  s.summary          = 'By far the most fantastic view I have seen in my entire life. No joke.'

  s.description      = <<-DESC
This fantastic view changes its color gradually makes your app look fantastic!
                       DESC

  s.homepage         = 'https://bitbucket.org/devTanya/fantasticview'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tanya' => 'tlandsman@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/devTanya/fantasticview.git', :tag => "#{s.version}"}

  s.ios.deployment_target = '10.0'
  s.source_files = 'FantasticView/FantasticView.swift'




end
